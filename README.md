# qemu-xen-aarch64

```shell
qemu-system-aarch64 -gdb tcp::3333 \
	-machine virt,gic_version=3 -machine virtualization=true \
	-cpu cortex-a53 -smp 4 -m 2G -nographic \
	-netdev user,id=net0,hostfwd=tcp::2222-:22 -device virtio-net-device,netdev=net0 \
	-kernel ./xen \
	-device loader,file=./Image,addr=0x40600000 \
	-drive if=virtio,file=./dom0.img,format=raw \
	-dtb ./virt-gicv3.dtb
```
* User: calinyara, Passwd: calinyara
